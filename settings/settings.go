package settings

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"fdps/fdps-statistics/db"
	"fdps/fdps-statistics/userhub"
	"fdps/utils"
)

// Settings настройки сервиса
type Settings struct {
	DbSetts db.Settings      `json:"DB"`
	WsSetts userhub.Settings `json:"WS"`
}

var settingsFile = utils.AppPath() + "\\config\\settings.json"

// ReadFromFile чтение ранее сохраненных настроек из файла
func (s *Settings) ReadFromFile() error {
	data, err := ioutil.ReadFile(settingsFile)
	if err != nil {
		return err
	}
	if err := json.Unmarshal(data, &s); err != nil {
		fmt.Println(err.Error())
		return err
	}
	return nil
}

// SaveToFile сохранение настроек в файл
func (s *Settings) SaveToFile() error {
	confData, err := json.Marshal(s)
	if err != nil {
		return err
	}
	if err := ioutil.WriteFile(settingsFile, utils.JsonPrettyPrint(confData), os.ModePerm); err != nil {
		return err
	}
	return nil
}

// DefaultInit настройки по умолчанию
func (s *Settings) DefaultInit() {
	s.DbSetts.InitByDefault()

	s.WsSetts.Port = utils.StatisticsClientsPort
}
