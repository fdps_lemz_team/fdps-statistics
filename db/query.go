package db

import "database/sql"

// QueryData структура для описания sql запроса к БД
type QueryData struct {
	QueryText  string
	SelectData bool
	Func       func(*sql.Rows) (interface{}, error)
}
