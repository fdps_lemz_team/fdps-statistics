package userhub

// AirportInfo
type AirportInfo struct {
	IcaoLat4 string
	IcaoRus4 string
}

// AirportInfoMap
type AirportInfoMap map[int]AirportInfo // ключ - id аэродрома

func (a AirportInfoMap) getIdByName(arptName string) (int, bool) {
	for key := range a {
		if a[key].IcaoLat4 == arptName || a[key].IcaoRus4 == arptName {
			return key, true
		}
	}
	return 0, false
}

// SectorInfo
type SectorInfo struct {
	Code      string
	SectType  string
	AirportId int
}

// SectorInfoMap
type SectorInfoMap map[int]SectorInfo // ключ - id сектора

// AirwayInfo
type AirwayInfo struct {
	IcaoLat6   string
	IcaoRus6   string
	AirwayType int
	PointIds   map[int]int // ключ - номер точки, значение - id точки
}

// AirwayInfoMap контейнер трасс
type AirwayInfoMap map[int]AirwayInfo

// по маршруту отпределить списко трасс, по которым проходит маршрут
// func (arw AirwayInfoMap) containsRoute(rt RawRoute) []string {
// 	retValue := make([]string, 0)

// 	rtPntIds := make(map[int]bool) // ключ - id точки, значение - содержание в трассе
// 	for rtKey := range rt {
// 		rtPntIds[rt[rtKey].PointId] = false
// 	}

// 	for key := range arw {
// 		for pntArwKey := range arw[key].PointIds {
// 			// если в map точек маршрута есть с таким же id
// 			if _, ok := rtPntIds[arw[key].PointIds[pntArwKey]]; ok {
// 				rtPntIds[arw[key].PointIds[pntArwKey]] = true
// 			}
// 		}

// 		// если в трассе встречается две точки, значит маршрут проходит через трассу
// 		countCoincidence := 0
// 		for rtPntKey := range rtPntIds {
// 			if rtPntIds[rtPntKey] == true {
// 				countCoincidence++
// 				// сбрасываем попадания точек для следующей трассы
// 				rtPntIds[rtPntKey] = false
// 			}
// 		}

// 		if countCoincidence >= 2 {
// 			logger.PrintfWarn("rt %v arw %v", rt, arw)
// 			retValue = append(retValue, arw[key].IcaoLat6)
// 		}
// 	}

// 	return retValue
// }

// PointInfo
type PointInfo struct {
	IcaoLat5 string
	IcaoRus5 string
}

// PointInfoMap контейнер точек
type PointInfoMap map[int]PointInfo // ключ - id точки
