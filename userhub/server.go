package userhub

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"fdps/utils"
	"fdps/utils/logger"
	"fdps/utils/userhub_web"
	"fdps/utils/web_sock"

	"github.com/jwangsadinata/go-multimap/slicemultimap"
)

const (
	srvStateKey       = "WS. Состояние:"
	srvLastConnKey    = "WS. Последнее клиентское подключение:"
	srvLastDisconnKey = "WS. Последнее клиентское отключение:"
	srvLastErrKey     = "WS. Последняя ошибка:"
	srvClntListKey    = "WS. Список клиентов:"

	srvStateOkValue    = "Запущен."
	srvStateErrorValue = "Не запущен."

	timeFormat = "2006-01-02 15:04:05"
)

// Settings настройки WS сервера
type Settings struct {
	Port int `json:"Port"`
}

// Server
type Server struct {
	SettsChan        chan Settings
	setts            Settings
	doneChan         chan struct{}
	wsServer         *web_sock.WebSockServer
	SelectedDateChan chan interface{}
	//AirportChan   chan []AirportInfo
	// SectorChan    chan []SectorInfo
	// AirwayChan    chan AirwayInfoMap
	// PointChan     chan []PointInfo
	TimePlansChan chan time.Time // канал для отправки даты запрашиваемых планов

	AirportMap         AirportInfoMap
	SectorMap          SectorInfoMap
	AirwayMap          AirwayInfoMap
	PointMap           PointInfoMap
	RawPlans           RawPlansMap
	AirwayMapForSearch *slicemultimap.MultiMap
}

// NewServer конструктор
func NewServer(done chan struct{}) *Server {
	return &Server{
		SettsChan:          make(chan Settings, 10),
		doneChan:           done,
		wsServer:           web_sock.NewWebSockServer(done),
		SelectedDateChan:   make(chan interface{}, 1),
		TimePlansChan:      make(chan time.Time, 1),
		AirwayMapForSearch: slicemultimap.New(),
	}
}

// Work реализация работы
func (s *Server) Work() {
	go s.wsServer.Work("/" + utils.StatisticsClientsPath)

	logger.SetDebugParam(srvStateKey, srvStateErrorValue+"   "+time.Now().Format(timeFormat), logger.StateErrorColor)
	logger.SetDebugParam(srvLastConnKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(srvLastDisconnKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(srvLastErrKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(srvClntListKey, "-", logger.StateDefaultColor)

	// sendAirportsFunc := func(sock *websocket.Conn) {
	// 	if data, err := json.Marshal(CreateAnswerAirportsMsg(s.AirportList)); err == nil {
	// 		s.wsServer.SendDataChan <- web_sock.WsPackage{Data: data, Sock: sock}
	// 	}
	// }

	// sendSectorsFunc := func(sock *websocket.Conn) {
	// 	if data, err := json.Marshal(CreateAnswerSectorsMsg(s.SectorList)); err == nil {
	// 		s.wsServer.SendDataChan <- web_sock.WsPackage{Data: data, Sock: sock}
	// 	}
	// }

	for {
		select {
		// получены новые настройки
		case newSetts := <-s.SettsChan:
			if newSetts != s.setts {
				s.setts = newSetts
				s.wsServer.SettingsChan <- web_sock.WebSockServerSettings{Port: s.setts.Port}
			}

		// получены данные из БД контроллера
		case curSelectedData := <-s.SelectedDateChan:
			if airportMap, ok := curSelectedData.(AirportInfoMap); ok {
				s.AirportMap = airportMap
				logger.PrintfWarn("airports: %v", len(s.AirportMap))
			} else if sectorMap, ok := curSelectedData.(SectorInfoMap); ok {
				s.SectorMap = sectorMap
				logger.PrintfWarn("sectors: %v", len(s.SectorMap))
			} else if airwayMap, ok := curSelectedData.(AirwayInfoMap); ok {
				s.AirwayMap = airwayMap
				s.processAirwaysSearchMap()
				logger.PrintfWarn("airways: %v", len(s.AirwayMap))
			} else if pointMap, ok := curSelectedData.(PointInfoMap); ok {
				s.PointMap = pointMap
				logger.PrintfWarn("points: %v", len(s.PointMap))
			} else if rawPlansMap, ok := curSelectedData.(RawPlansMap); ok {
				s.RawPlans = rawPlansMap
				logger.PrintfWarn("rawPlans: %v", len(s.RawPlans))

				if data, err := json.Marshal(CreateAnswerPlansMsg(s.ProcessRawPlans())); err == nil {
					s.wsServer.SendDataChan <- web_sock.WsPackage{Data: data}
					//logger.PrintfErr("%v", string(data))
				} else {
					logger.PrintfErr("%v", err)
				}

			} else {
				logger.PrintfErr("неизвестный тип")
			}

		// получен подключенный клиент от WS сервера
		case curClnt := <-s.wsServer.ClntConnChan:
			logger.PrintfInfo("Подключен клиент с адресом: %s.", curClnt.RemoteAddr().String())
			logger.SetDebugParam(srvLastConnKey, curClnt.RemoteAddr().String()+" "+time.Now().Format(timeFormat), logger.StateDefaultColor)
			logger.SetDebugParam(srvClntListKey, fmt.Sprintf("%v", s.wsServer.ClientList()), logger.StateDefaultColor)
			userhub_web.ClientConn(userhub_web.FromWebSock(curClnt))

		// получен отключенный клиент от WS сервера
		case curClnt := <-s.wsServer.ClntDisconnChan:
			logger.PrintfInfo("Отключен клиентс адресом: %s.", curClnt.RemoteAddr().String())
			logger.SetDebugParam(srvLastDisconnKey, curClnt.RemoteAddr().String()+" "+time.Now().Format(timeFormat), logger.StateDefaultColor)
			logger.SetDebugParam(srvClntListKey, fmt.Sprintf("%v", s.wsServer.ClientList()), logger.StateDefaultColor)
			userhub_web.ClientDisconn(userhub_web.FromWebSock(curClnt))

		// получен отклоненный клиент от WS сервера
		case curClnt := <-s.wsServer.ClntRejectChan:
			logger.PrintfInfo("Отклонен клиент с адресом: %s.", curClnt.RemoteAddr().String())

		// получена ошибка от WS сервера
		case wsErr := <-s.wsServer.ErrorChan:
			logger.PrintfErr("Возникла ошибка при работе WS сервера. Ошибка: %s.", wsErr.Error())
			logger.SetDebugParam(srvLastErrKey, wsErr.Error(), logger.StateErrorColor)

		// получены данные от WS сервера
		case curWsPkg := <-s.wsServer.ReceiveDataChan:
			var curHdr HeaderMsg
			var unmErr error
			if unmErr = json.Unmarshal(curWsPkg.Data, &curHdr); unmErr == nil {
				switch curHdr.Header {

				case RequestPlansHdr:
					logger.PrintfWarn("RequestPlansMsg")
					var curMsg RequestPlansMsg
					if err := json.Unmarshal(curWsPkg.Data, &curMsg); err == nil {
						logger.PrintfWarn("RequestPlansMsg.Date %v", curMsg.Date)

						if cutTime, errParse := time.Parse(dateFormat, curMsg.Date); errParse == nil {
							logger.PrintfWarn("датa %v", cutTime)
							s.TimePlansChan <- cutTime
						} else {
							logger.PrintfErr("Ошибка разбора даты в сообщении запроса статистики %v", errParse)
						}

					} else {
						logger.PrintfErr("error parse %v", err)
					}

				case ClientHeartbeatHdr:
					var curHbtMsg ClientHeartbeatMsg
					if err := json.Unmarshal(curWsPkg.Data, &curHbtMsg); err == nil {
						userhub_web.ClientUpdate(curHbtMsg.ToUserhubClient(curWsPkg.Sock))
					}

					// case ClientParkingChangeHdr:
					// 	var curMsg ClientParkingChangeMsg
					// 	if err := json.Unmarshal(curWsPkg.Data, &curMsg); err == nil {
					// 		//s.ParkingChangeChan <- curMsg.Parking

					// 		// for ind, val := range s.Parkings {
					// 		// 	if val.Name == curMsg.Parking.Name {
					// 		// 		s.Parkings[ind] = curMsg.Parking
					// 		// 	}
					// 		// }

					// 		// уведомление всех клиентов
					// 		if parkingsData, err := json.Marshal(CreateServerParkingChangeMsg(&curMsg.Parking)); err == nil {
					// 			s.wsServer.SendDataChan <- web_sock.WsPackage{Data: parkingsData}
					// 		}
					// 	}
				}

			} else {
				logger.PrintfErr("Ошибка разбора сообщения от приложения AFTN канала. Ошибка: %s.", unmErr)
			}

		// получено состояние работоспособности WS сервера для связи с AFTN каналами
		case connState := <-s.wsServer.StateChan:
			switch connState {
			case web_sock.ServerTryToStart:
				logger.PrintfInfo("Запускаем WS сервер для взаимодействия. Порт: %d Path: %s.", s.setts.Port, utils.ParkingClientsPath)
				logger.SetDebugParam(srvStateKey, srvStateOkValue+" Порт: "+strconv.Itoa(s.setts.Port)+" Path: "+utils.StatisticsClientsPath, logger.StateOkColor)
			case web_sock.ServerError:
				logger.SetDebugParam(srvStateKey, srvStateErrorValue, logger.StateErrorColor)
				//case web_sock.ServerReadySend:
				//case web_sock.ServerNotReadySend:
			}
		}
	}
}
