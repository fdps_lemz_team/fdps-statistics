package userhub

import (
	"fdps/fdps-statistics/consts"
	"sort"
	"time"
)

// участок трассы
type AirwaySearchPart struct {
	firstPntId  int // ID первой точка участка
	secondPntId int // ID второй точка участка
}

// map выдает ключи в разнобой, надо отсортировать по PointNo
type pntNumId struct {
	pntNum int
	pntId  int
	eto    string
}

// название трассы и время от-до, когда маршрут принадлежал трассе
type arwWithTime struct {
	arwName string
	etoMin  int // час минимального ETO
	etoMax  int // час максимального ETO
}

// наполнение мэпки для поиска трасс
func (s *Server) processAirwaysSearchMap() {
	s.AirwayMapForSearch.Clear()

	for key := range s.AirwayMap {

		rtPntIds := make([]pntNumId, 0)
		for pntNoKey := range s.AirwayMap[key].PointIds {
			rtPntIds = append(rtPntIds, pntNumId{pntNum: pntNoKey, pntId: s.AirwayMap[key].PointIds[pntNoKey]})
		}

		sort.Slice(rtPntIds, func(i, j int) bool { return rtPntIds[i].pntNum < rtPntIds[j].pntNum })

		for nn := 0; nn < len(rtPntIds)-1; nn++ {
			s.AirwayMapForSearch.Put(
				AirwaySearchPart{firstPntId: rtPntIds[nn].pntId, secondPntId: rtPntIds[nn+1].pntId},
				s.AirwayMap[key].IcaoLat6,
			)
		}
	}
}

// поиск трасс, через которые проходит маршрут
func (s *Server) airwaysForRoute(rt RawRoute) []arwWithTime {
	tmpRetValue := make([]arwWithTime, 0)

	rtPntIds := make([]pntNumId, 0)
	for rtKey := range rt {
		rtPntIds = append(rtPntIds, pntNumId{
			pntNum: rtKey,
			pntId:  rt[rtKey].PointId,
			eto:    rt[rtKey].ETO,
		})
	}

	sort.Slice(rtPntIds, func(i, j int) bool { return rtPntIds[i].pntNum < rtPntIds[j].pntNum })

	for pntIt := 0; pntIt < len(rtPntIds)-1; pntIt++ {

		if rtPntIds[pntIt].pntId != 0 && rtPntIds[pntIt+1].pntId != 0 {
			firstPntEto, errFirstParse := time.Parse(consts.TimeFromDbFormat, rtPntIds[pntIt].eto)
			secondPntEto, errSecondParse := time.Parse(consts.TimeFromDbFormat, rtPntIds[pntIt+1].eto)

			if errFirstParse == nil && errSecondParse == nil {

				// поиск по маршруту в прямом направлении
				rtSlice, found := s.AirwayMapForSearch.Get(AirwaySearchPart{
					rtPntIds[pntIt].pntId,
					rtPntIds[pntIt+1].pntId,
				})
				if found {
					for _, rtSliceIt := range rtSlice {
						if _, ok := rtSliceIt.(string); ok {

							tmpRetValue = append(tmpRetValue, arwWithTime{
								arwName: rtSliceIt.(string),
								etoMin:  firstPntEto.Hour(),
								etoMax:  secondPntEto.Hour(),
							})
						}
					}
				}

				// поиск по маршруту в обратном направлении
				rtSliceBack, foundBack := s.AirwayMapForSearch.Get(AirwaySearchPart{
					rtPntIds[pntIt+1].pntId,
					rtPntIds[pntIt].pntId,
				})
				if foundBack {
					for _, rtSliceIt := range rtSliceBack {
						if _, ok := rtSliceIt.(string); ok {

							tmpRetValue = append(tmpRetValue, arwWithTime{
								arwName: rtSliceIt.(string),
								etoMin:  firstPntEto.Hour(),
								etoMax:  secondPntEto.Hour(),
							})
						}
					}
				}

			}
		}
	}

	// удаление дубликатов
	uniqueAirways := make(map[string]arwWithTime)
	for _, val := range tmpRetValue {
		if _, ok := uniqueAirways[val.arwName]; ok {
			if uniqueAirways[val.arwName].etoMin > val.etoMin {
				uniqueAirways[val.arwName] = arwWithTime{}
			}
			if uniqueAirways[val.arwName].etoMin > val.etoMin {
				uniqueAirways[val.arwName] = arwWithTime{}
			}
		} else {
			uniqueAirways[val.arwName] = val
		}
	}

	retValue := make([]arwWithTime, 0)
	for uniqueKey := range uniqueAirways {
		retValue = append(retValue, uniqueAirways[uniqueKey])
	}

	return retValue
}
