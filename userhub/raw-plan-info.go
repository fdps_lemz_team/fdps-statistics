package userhub

// RawRoutePoint - маргрут необработанного плана
type RawRoutePoint struct {
	//PointNo   int
	SectorId  int
	PointId   int
	PointCode string
	ETO       string
	ATO       string
}

type RawRoute map[int]RawRoutePoint // ключ - порядковый номер точки

// RawPlan - информация о необработанном плане
type RawPlan struct {
	PlanProcess int
	InTime      string
	OutTime     string
	DepAirport  string
	DestAirport string
	InPoint     string
	OutPoint    string
	Route       string
	Sectors     string
	ETD         string
	ETA         string
	RoutePoints RawRoute // ключ - порядковый номер точки в маршруте, значение - точка
}

// RawPlansMap - кнтейнер необработанных планов
type RawPlansMap map[int]RawPlan
