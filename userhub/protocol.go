package userhub

import (
	"fdps/utils/userhub_web"

	"github.com/gorilla/websocket"
)

const (
	// RequestPlansHdr заголовок сообщения запроса списка планов
	RequestPlansHdr = "RequestPlans"

	// AnswerPlansHdr заголовок сообщения со списком планов
	AnswerPlansHdr = "AnswerPlans"

	// ClientHeartbeatHdr заголовок сообщения о состоянии клиента
	ClientHeartbeatHdr = "ClientHeartbeat"

	dateFormat = "2006-01-02"
)

// HeaderMsg описание заголовка сообщений
type HeaderMsg struct {
	Header string `json:"MessageType"`
}

// RequestPlansMsg сообщение запроса списка планов
// клиент -> сервер
type RequestPlansMsg struct {
	HeaderMsg
	Date string `json:"Date"`
}

// AnswerPlansMsg сообщения со списком планов
// сервер -> клиент
type AnswerPlansMsg struct {
	HeaderMsg
	Plans PlansInfo `json:"PlansInfo"`
}

// CreateAnswerPlansMsg сформировать сообщение со списком планов
func CreateAnswerPlansMsg(p PlansInfo) AnswerPlansMsg {
	return AnswerPlansMsg{HeaderMsg: HeaderMsg{Header: AnswerPlansHdr}, Plans: p}
}

// ClientHeartbeatMsg сообщение о состоянии канала
// клиент -> сервер
type ClientHeartbeatMsg struct {
	HeaderMsg
	IPs     []string `json:"IPs"`     // список IP адресов клиентского приложения
	Key     string   `json:"Key"`     // уникальный ключ клиента
	Name    string   `json:"Name"`    // название клиентского приложения
	Version string   `json:"Version"` // версия ПО клиента
	Time    string   `json:"-"`       // время получения данных
}

// ToUserhubClient
func (hbt *ClientHeartbeatMsg) ToUserhubClient(ws *websocket.Conn) userhub_web.Client {
	retValue := userhub_web.Client{
		Sock:    ws,
		Key:     hbt.Key,
		AppName: hbt.Name,
		IpAddr:  hbt.IPs,
		Version: hbt.Version,
	}
	retValue.MarkTime()

	return retValue
}
