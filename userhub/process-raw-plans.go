package userhub

import (
	"fdps/fdps-statistics/consts"
	"time"
)

func (s *Server) createEmptyPlansInfo() PlansInfo {
	var plans PlansInfo

	plans.SectorPlans = make(map[string]Sector)
	for _, val := range s.SectorMap {
		plans.SectorPlans[val.Code] = makeEmptyRc()
	}

	plans.AirdromPlans = make(map[string]Airport)
	for _, val := range s.AirportMap {
		plans.AirdromPlans[val.IcaoLat4] = makeEmptyAirport()
	}

	plans.AirwayPlans = make(map[string]Airway)
	for key := range s.AirwayMap {
		plans.AirwayPlans[s.AirwayMap[key].IcaoLat6] = makeEmptyAirway()
	}

	plans.PointPlans = make(map[string]PlanProcType)
	for key := range s.PointMap {
		plans.PointPlans[s.PointMap[key].IcaoLat5] = makeEmptyPlanProc()
	}

	return plans
}

func (s *Server) ProcessRawPlans() PlansInfo {
	plans := s.createEmptyPlansInfo()

	for key := range s.RawPlans {

		_, okDep := s.AirportMap.getIdByName(s.RawPlans[key].DepAirport)
		_, okArr := s.AirportMap.getIdByName(s.RawPlans[key].DepAirport)

		curDestType := planDestTransit

		if okDep {
			if curDepTime, errParse := time.Parse(consts.TimeFromDbFormat, s.RawPlans[key].InTime); errParse == nil {

				plans.processAirport(planDestDep, s.RawPlans[key].DepAirport, s.RawPlans[key].PlanProcess, curDepTime.Hour())

				curDestType = planDestDep
			}
		}
		if okArr {
			if curArrTime, errParse := time.Parse(consts.TimeFromDbFormat, s.RawPlans[key].OutTime); errParse == nil {

				plans.processAirport(planDestArr, s.RawPlans[key].DestAirport, s.RawPlans[key].PlanProcess, curArrTime.Hour())

				curDestType = planDestArr
			}
		}

		plans.processRouteForPoints(curDestType, s.RawPlans[key].PlanProcess, s.RawPlans[key].RoutePoints)
		plans.processRouteForSectors(curDestType, s.RawPlans[key].PlanProcess, s.RawPlans[key].RoutePoints, s.SectorMap)

		if curAirways := s.airwaysForRoute(s.RawPlans[key].RoutePoints); len(curAirways) != 0 {
			plans.processRouteForAirways(curDestType, s.RawPlans[key].PlanProcess, curAirways)
		}
	}

	plans.erasePointsWithEmptyStats()
	plans.eraseAirwaysWithEmptyStats()
	plans.eraseSectorsWithEmptyStats()

	return plans
}

// обработка маршрута
func (plnInf *PlansInfo) processRouteForPoints(destType planDestType, planProcess int, rt RawRoute) {

	for key := range rt {
		if curEto, errParse := time.Parse(consts.TimeFromDbFormat, rt[key].ETO); errParse == nil {
			if rt[key].PointId != consts.InvalidNumber && rt[key].PointCode != "" {

				if _, ok := plnInf.PointPlans[rt[key].PointCode]; ok {
					switch planProcess {
					case performPlanProcess:
						plnInf.PointPlans[rt[key].PointCode].PerformCount[curEto.Hour()] += 1
					case cancelPlanProcess:
						plnInf.PointPlans[rt[key].PointCode].CancelCount[curEto.Hour()] += 1
					case unperformProcess:
						plnInf.PointPlans[rt[key].PointCode].UnperformCount[curEto.Hour()] += 1
					}
					plnInf.PointPlans[rt[key].PointCode].DailyCount[curEto.Hour()] += 1
				}
			}
		}
	}
}

func (plnInf *PlansInfo) processRouteForSectors(destType planDestType, planProcess int, rt RawRoute, sectorMap SectorInfoMap) {
	// пройденные маршрутом сетора.
	// ключ - код сектора, значение - ETO последней точки маршрута, принадлежащей сектору
	passedSectors := make(map[string]int)

	// пробегаем по маршруту и ищем точки указанным сектором
	for key := range rt {
		if rt[key].SectorId != consts.InvalidNumber {
			if curSec, okSector := sectorMap[rt[key].SectorId]; okSector {
				if curEto, errParse := time.Parse(consts.TimeFromDbFormat, rt[key].ETO); errParse == nil {
					passedSectors[curSec.Code] = curEto.Hour()
				}
			}
		}
	}

	for key := range passedSectors {
		//if _, ok := plnInf.SectorPlans[key]; ok {
		switch planProcess {
		case performPlanProcess:
			switch destType {
			case planDestDep:
				plnInf.SectorPlans[key].DepCount.PerformCount[passedSectors[key]] += 1
			case planDestArr:
				plnInf.SectorPlans[key].ArrCount.PerformCount[passedSectors[key]] += 1
			case planDestTransit:
				plnInf.SectorPlans[key].TransitCount.PerformCount[passedSectors[key]] += 1
			}
			plnInf.SectorPlans[key].TotalCount.PerformCount[passedSectors[key]] += 1
		case cancelPlanProcess:

			switch destType {
			case planDestDep:
				plnInf.SectorPlans[key].DepCount.CancelCount[passedSectors[key]] += 1
			case planDestArr:
				plnInf.SectorPlans[key].ArrCount.CancelCount[passedSectors[key]] += 1
			case planDestTransit:
				plnInf.SectorPlans[key].TransitCount.CancelCount[passedSectors[key]] += 1
			}
			plnInf.SectorPlans[key].TotalCount.CancelCount[passedSectors[key]] += 1

		case unperformProcess:

			switch destType {
			case planDestDep:
				plnInf.SectorPlans[key].DepCount.UnperformCount[passedSectors[key]] += 1
			case planDestArr:
				plnInf.SectorPlans[key].ArrCount.UnperformCount[passedSectors[key]] += 1
			case planDestTransit:
				plnInf.SectorPlans[key].TransitCount.UnperformCount[passedSectors[key]] += 1
			}
			plnInf.SectorPlans[key].TotalCount.UnperformCount[passedSectors[key]] += 1
		}
		plnInf.SectorPlans[key].TotalCount.DailyCount[passedSectors[key]] += 1
	}
}

func (plnInf *PlansInfo) processRouteForAirways(destType planDestType, planProcess int, arw []arwWithTime) {

	for _, val := range arw {

		for curHour := val.etoMin; curHour <= val.etoMax; curHour++ {
			switch planProcess {
			case performPlanProcess:

				switch destType {
				case planDestDep:
					plnInf.AirwayPlans[val.arwName].DepCount.PerformCount[curHour] += 1
				case planDestArr:
					plnInf.AirwayPlans[val.arwName].ArrCount.PerformCount[curHour] += 1
				case planDestTransit:
					plnInf.AirwayPlans[val.arwName].TransitCount.PerformCount[curHour] += 1
				}
				plnInf.AirwayPlans[val.arwName].TotalCount.PerformCount[curHour] += 1
			case cancelPlanProcess:

				switch destType {
				case planDestDep:
					plnInf.AirwayPlans[val.arwName].DepCount.CancelCount[curHour] += 1
				case planDestArr:
					plnInf.AirwayPlans[val.arwName].ArrCount.CancelCount[curHour] += 1
				case planDestTransit:
					plnInf.AirwayPlans[val.arwName].TransitCount.CancelCount[curHour] += 1
				}
				plnInf.AirwayPlans[val.arwName].TotalCount.CancelCount[curHour] += 1

			case unperformProcess:

				switch destType {
				case planDestDep:
					plnInf.AirwayPlans[val.arwName].DepCount.UnperformCount[curHour] += 1
				case planDestArr:
					plnInf.AirwayPlans[val.arwName].ArrCount.UnperformCount[curHour] += 1
				case planDestTransit:
					plnInf.AirwayPlans[val.arwName].TransitCount.UnperformCount[curHour] += 1
				}
				plnInf.AirwayPlans[val.arwName].TotalCount.UnperformCount[curHour] += 1
			}
			plnInf.AirwayPlans[val.arwName].TotalCount.DailyCount[curHour] += 1
		}
	}

}
