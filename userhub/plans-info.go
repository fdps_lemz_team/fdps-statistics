package userhub

const (
	//-- суточный			@ppMin = 201	@ppMax = 500
	performPlanProcess = 402
	cancelPlanProcess  = 404
	unperformProcess   = 405
)

type planDestType int

const (
	planDestDep planDestType = iota
	planDestArr
	planDestTransit
)

// PlanProcType по типу обработки плана
type PlanProcType struct {
	DailyCount     map[int]int `json:"Daily"`
	PerformCount   map[int]int `json:"Perform"`
	CancelCount    map[int]int `json:"Cancel"`
	UnperformCount map[int]int `json:"Unperform"`
}

// Sector параметры по сектору
type Sector struct {
	ArrCount     PlanProcType `json:"Arr"`
	DepCount     PlanProcType `json:"Dep"`
	TransitCount PlanProcType `json:"Transit"`
	TotalCount   PlanProcType `json:"Total"`
}

// Airport параметры по аэродрому
type Airport struct {
	ArrCount   PlanProcType `json:"Arr"`
	DepCount   PlanProcType `json:"Dep"`
	TotalCount PlanProcType `json:"Total"`
}

// Airway параметры по трассе
type Airway struct {
	ArrCount     PlanProcType `json:"Arr"`
	DepCount     PlanProcType `json:"Dep"`
	TransitCount PlanProcType `json:"Transit"`
	TotalCount   PlanProcType `json:"Total"`
}

// PlansInfo общая информация за сутки
type PlansInfo struct {
	SectorPlans  map[string]Sector       `json:"Rc"`
	PointPlans   map[string]PlanProcType `json:"Point"`
	AirdromPlans map[string]Airport      `json:"Airdrom"`
	AirwayPlans  map[string]Airway       `json:"Airway"`
}

func makeEmptyMap() map[int]int {
	return map[int]int{0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 0, 13: 0, 14: 0, 15: 0, 16: 0, 17: 0, 18: 0, 19: 0, 20: 0, 21: 0, 22: 0, 23: 0}
}

func makeEmptyPlanProc() PlanProcType {
	return PlanProcType{
		DailyCount:     makeEmptyMap(),
		PerformCount:   makeEmptyMap(),
		CancelCount:    makeEmptyMap(),
		UnperformCount: makeEmptyMap(),
	}
}

func makeEmptyRc() Sector {
	return Sector{
		ArrCount:     makeEmptyPlanProc(),
		DepCount:     makeEmptyPlanProc(),
		TransitCount: makeEmptyPlanProc(),
		TotalCount:   makeEmptyPlanProc(),
	}
}

func makeEmptyAirport() Airport {
	return Airport{
		ArrCount:   makeEmptyPlanProc(),
		DepCount:   makeEmptyPlanProc(),
		TotalCount: makeEmptyPlanProc(),
	}
}

func makeEmptyAirway() Airway {
	return Airway{
		ArrCount:     makeEmptyPlanProc(),
		DepCount:     makeEmptyPlanProc(),
		TransitCount: makeEmptyPlanProc(),
		TotalCount:   makeEmptyPlanProc(),
	}
}

func (plnInf *PlansInfo) processAirport(destType planDestType, airportName string, planProcess int, hour int) {
	if _, ok := plnInf.AirdromPlans[airportName]; ok {

		switch planProcess {
		case performPlanProcess:
			switch destType {
			case planDestDep:
				plnInf.AirdromPlans[airportName].DepCount.PerformCount[hour] += 1
			case planDestArr:
				plnInf.AirdromPlans[airportName].ArrCount.PerformCount[hour] += 1
			}

			plnInf.AirdromPlans[airportName].TotalCount.PerformCount[hour] += 1

		case cancelPlanProcess:
			switch destType {
			case planDestDep:
				plnInf.AirdromPlans[airportName].DepCount.CancelCount[hour] += 1
			case planDestArr:
				plnInf.AirdromPlans[airportName].ArrCount.CancelCount[hour] += 1
			}
			plnInf.AirdromPlans[airportName].TotalCount.CancelCount[hour] += 1

		case unperformProcess:
			switch destType {
			case planDestDep:
				plnInf.AirdromPlans[airportName].DepCount.UnperformCount[hour] += 1
			case planDestArr:
				plnInf.AirdromPlans[airportName].ArrCount.UnperformCount[hour] += 1
			}
			plnInf.AirdromPlans[airportName].TotalCount.UnperformCount[hour] += 1
		}
		// суточный всегда ++
		switch destType {
		case planDestDep:
			plnInf.AirdromPlans[airportName].DepCount.DailyCount[hour] += 1
		case planDestArr:
			plnInf.AirdromPlans[airportName].ArrCount.DailyCount[hour] += 1
		}
		plnInf.AirdromPlans[airportName].TotalCount.DailyCount[hour] += 1
	}
}

// удаление точек, через которые не было полетов
func (plnInf *PlansInfo) erasePointsWithEmptyStats() {
	for key := range plnInf.PointPlans {
		emptyStats := true

	PntLoop:
		for timeIt := 0; timeIt < 24; timeIt++ {
			if plnInf.PointPlans[key].DailyCount[timeIt] != 0 {
				emptyStats = false
				break PntLoop
			}
		}

		if emptyStats == true {
			delete(plnInf.PointPlans, key)
		}
	}
}

// удаление трасс, через которые не было полетов
func (plnInf *PlansInfo) eraseAirwaysWithEmptyStats() {
	for key := range plnInf.AirwayPlans {
		emptyStats := true

	PntLoop:
		for timeIt := 0; timeIt < 24; timeIt++ {
			if plnInf.AirwayPlans[key].TotalCount.DailyCount[timeIt] != 0 {
				emptyStats = false
				break PntLoop
			}
		}

		if emptyStats == true {
			delete(plnInf.AirwayPlans, key)
		}
	}
}

// удаление секторов, через которые не было полетов
func (plnInf *PlansInfo) eraseSectorsWithEmptyStats() {
	for key := range plnInf.SectorPlans {
		emptyStats := true

	PntLoop:
		for timeIt := 0; timeIt < 24; timeIt++ {
			if plnInf.SectorPlans[key].TotalCount.DailyCount[timeIt] != 0 {
				emptyStats = false
				break PntLoop
			}
		}

		if emptyStats == true {
			delete(plnInf.SectorPlans, key)
		}
	}
}
