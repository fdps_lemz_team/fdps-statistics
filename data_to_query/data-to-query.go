package data_to_query

import (
	"fmt"
	"time"

	"fdps/fdps-statistics/consts"
	"fdps/fdps-statistics/db"
)

// SelectAirportsToQuery - запрос списка аэродромов РЦ
func SelectAirportsToQuery() []db.QueryData {
	return []db.QueryData{db.QueryData{
		QueryText:  fmt.Sprintf(`select airportid, icaolat4, icaorus4 from airports where isinfir = 1`),
		SelectData: true,
		Func:       db.SelectAirportsFunc,
	}}
}

// SelectSectorsToQuery - запрос списка секторов
func SelectSectorsToQuery() []db.QueryData {
	return []db.QueryData{db.QueryData{
		QueryText:  fmt.Sprintf(`select sectorid, sectorcode, secttype, nvl(arptid, 0) from sectors`),
		SelectData: true,
		Func:       db.SelectSectorsFunc,
	}}
}

// SelectAirwaysToQuery - запрос списка трасс
func SelectAirwaysToQuery() []db.QueryData {
	return []db.QueryData{db.QueryData{
		QueryText: fmt.Sprintf(`select arw.airwayid, arw.icaolat6, arw.icaorus6, arw.airwaytype, arwp.pointid, arwp.pointno 		
		from airways arw left join airwaypoints arwp on arw.airwayid = arwp.airwayid order by arw.airwayid, arwp.pointno`),
		SelectData: true,
		Func:       db.SelectAirwaysFunc,
	}}
}

// SelectPointsToQuery - запрос списка точек
func SelectPointsToQuery(firCode string) []db.QueryData {
	return []db.QueryData{db.QueryData{
		//QueryText:  fmt.Sprintf(`SELECT POINTID, ICAOLAT5, ICAORUS5 FROM POINTS WHERE FIRCODE LIKE '%s%s'`, firCode, "%"),
		QueryText:  fmt.Sprintf(`select pointid, icaolat5, icaorus5 from points`),
		SelectData: true,
		Func:       db.SelectPointsFunc,
	}}
}

// SelectPlansToQuery - запрос списка планов за день
func SelectPlansToQuery(plansTime time.Time) []db.QueryData {
	beginTime := time.Date(plansTime.Year(), plansTime.Month(), plansTime.Day(), 0, 0, 0, 0, time.UTC)
	beginSecs := beginTime.Unix()

	endTime := time.Date(plansTime.Year(), plansTime.Month(), plansTime.Day(), 23, 59, 59, 0, time.UTC)
	endSecs := endTime.Unix()

	return []db.QueryData{db.QueryData{
		QueryText: fmt.Sprintf(`select 
		p.planid
	  , p.planprocess
	  , nvl(to_char(p.intime2, 'yyyy-mm-dd hh24:mi'), '%s')
	  , nvl(to_char(p.outtime2, 'yyyy-mm-dd hh24:mi'), '%s')
	  , nvl(p.depairport, '')
	  , nvl(p.destairport, '')
	  , nvl(p.inpoint, '')
	  , nvl(p.outpoint, '')
	  , nvl(p.route, '')
	  , nvl(p.sectors, '')	
	  , nvl(to_char(p.etd2, 'yyyy-mm-dd hh24:mi'), '%s')
	  , nvl(to_char(p.eta2, 'yyyy-mm-dd hh24:mi'), '%s')
	  , nvl(r.pointno, %d)
	  , nvl(r.sectorid, %d)
	  , nvl(r.pointid, %d)
	  , nvl(r.pointcode, '')
	  , nvl(to_char(r.eto, 'yyyy-mm-dd hh24:mi'), '%s')
	  , nvl(to_char(r.ato, 'yyyy-mm-dd hh24:mi'), '%s') 
	  from plans p 
	  left join routes r on p.planid=r.planid
	  
	  where p.intime >= %d AND p.intime <= %d  
	  order by p.planid, r.pointno`,
			consts.InvalidDate,
			consts.InvalidDate,
			consts.InvalidDate,
			consts.InvalidDate,
			consts.InvalidNumber,
			consts.InvalidNumber,
			consts.InvalidNumber,
			consts.InvalidDate,
			consts.InvalidDate,
			beginSecs,
			endSecs),
		SelectData: true,
		Func:       db.SelectPlansFunc,
	}}
}
