package worker

import (
	"sync"

	"fdps/fdps-statistics/data_to_query"
	"fdps/fdps-statistics/db"
	"fdps/fdps-statistics/settings"
	"fdps/fdps-statistics/userhub"
	"fdps/fdps-statistics/web"
	"fdps/utils/logger"
)

var (
	akeyForSave string
	akeyForSend string
)

func Start(done chan struct{}, wg *sync.WaitGroup) {
	var setts settings.Settings
	if errRead := setts.ReadFromFile(); errRead != nil {
		logger.PrintfErr("Ошибка чтения настроек из файла: %v", errRead)
		setts.DefaultInit()
		logger.PrintfInfo("Настройки инициализирован значениями по умолчанию: %#v", setts)
		if errSave := setts.SaveToFile(); errSave != nil {
			logger.PrintfErr("Ошибка сохранения настроек в файл: %v", errSave)
		} else {
			logger.PrintfErr("Настройки сохранены в файл")
		}
	}

	// aniClient := ani.NewClient()
	// go aniClient.Work()
	// aniClient.SettsChan <- setts.AniSetts
	// web.SetAniSetts(setts.AniSetts)

	userServer := userhub.NewServer(done)
	go userServer.Work()
	userServer.SettsChan <- userhub.Settings{Port: setts.WsSetts.Port}
	web.SetWsSetts(userhub.Settings{Port: setts.WsSetts.Port})

	dbClient := db.NewController(done)
	go dbClient.Work()
	dbClient.ConnStringChan <- setts.DbSetts.ConnString()
	web.SetDbSetts(setts.DbSetts)
	// запрос секторов
	dbClient.QueryChan <- data_to_query.SelectSectorsToQuery()
	// запрос ародромов
	dbClient.QueryChan <- data_to_query.SelectAirportsToQuery()
	// запрос трасс
	dbClient.QueryChan <- data_to_query.SelectAirwaysToQuery()
	// запрос точек
	dbClient.QueryChan <- data_to_query.SelectPointsToQuery("UEEE")

	go func() {
		for {
			select {

			case setts.DbSetts = <-web.DbSettsChan:
				if errSave := setts.SaveToFile(); errSave != nil {
					logger.PrintfErr("Ошибка сохранения настроек: %v.")
				}
				dbClient.ConnStringChan <- setts.DbSetts.ConnString()

				// запрос секторов
				dbClient.QueryChan <- data_to_query.SelectSectorsToQuery()
				// запрос фэродромов
				dbClient.QueryChan <- data_to_query.SelectAirportsToQuery()

			case setts.WsSetts = <-web.WsSettsChan:
				if errSave := setts.SaveToFile(); errSave != nil {
					logger.PrintfErr("Ошибка сохранения настроек: %v.")
				}
				userServer.SettsChan <- setts.WsSetts

			case curSelectedData := <-dbClient.SelectedDateChan:
				userServer.SelectedDateChan <- curSelectedData

			case curDate := <-userServer.TimePlansChan:
				dbClient.QueryChan <- data_to_query.SelectPlansToQuery(curDate)

			case <-done:
				wg.Done()
				return

			}
		}
	}()
}
