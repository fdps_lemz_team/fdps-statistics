package main

import (
	"log"
	"sync"

	"fdps/fdps-statistics/web"
	"fdps/fdps-statistics/worker"
	"fdps/utils"
	"fdps/utils/logger"
	"fdps/utils/logger/log_ljack"
	"fdps/utils/logger/log_std"
	"fdps/utils/logger/log_web"
	"fdps/utils/userhub_web"
)

const (
	appName    = "fdps-statistics"
	appVersion = "2020-05-12 13:11"
)

var workWithDocker bool
var dockerVersion string

func initLoggers() {
	// логгер с web страничкой
	log_web.Initialize(log_web.LogWebSettings{
		StartHttp: false,
		//NetPort:      utils.ParkingWebDefaultPort,
		LogURLPath:   utils.StatisticsWebLogPath,
		Title:        appName,
		ShowSetts:    true,
		SettsURLPath: utils.StatisticsWebConfigPath,
	})
	logger.AppendLogger(log_web.WebLogger)
	utils.AppendHandler(log_web.WebLogger)
	log_web.SetVersion(appVersion)

	// логгер с сохранением в файлы
	var ljackSetts log_ljack.LjackSettings
	ljackSetts.GenDefault()
	ljackSetts.FilesName = appName + ".log"
	if err := log_ljack.Initialize(ljackSetts); err != nil {
		logger.PrintfErr("Ошибка инициализации логгера lumberjack. Ошибка: %s", err.Error())
	}
	logger.AppendLogger(log_ljack.LogLjack)

	logger.AppendLogger(log_std.LogStd)

	log.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds | log.LUTC | log.Llongfile)
}

func initDockerInfo() {
	if dockerVersion, dockErr := utils.GetDockerVersion(); dockErr != nil {
		log_web.SetDockerVersion("???")
		logger.PrintfErr(dockErr.Error())
	} else {
		workWithDocker = true
		log_web.SetDockerVersion(dockerVersion)
	}
}

func main() {

	initLoggers()
	initDockerInfo()

	userhub_web.Initialize(utils.StatisticsWebClientsPath, "STATISTICS-CLIENTS")
	utils.AppendHandler(userhub_web.ClntHdl)

	wg := sync.WaitGroup{}
	wg.Add(1)

	done := make(chan struct{})
	web.Start(done)

	go worker.Start(done, &wg)
	wg.Wait()
	logger.PrintfInfo("%s %s", appName, "stopped")
}
